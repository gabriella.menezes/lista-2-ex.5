
/**
 * Escreva a descrição da classe Jogador aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class Jogador extends Atleta
{
    private int Numero, Posicao;
    private String posição;
    public Jogador()
    {
        Numero=0;
        Posicao=0;
        posição="";
    }
    public String Posiçao()
    {
        if (Posicao==0)
        {
            return posição="Goleiro";
        }
        else if (Posicao==1)
        {
            return posição="Zagueiro";
        }
        else if (Posicao==2)
        {
            return posição="Meio-Campo";
        }
        else if (Posicao==3)
        {
            return posição="Ataque";
        }
        else
        {return posição="Sem posição definida";}
    }
    public void setNumero(int Numero)
    {
        this.Numero=Numero;
    }
    public int getNumero()
    {
        return Numero;
    }
    public void setPosicao(int Posicao)
    {
        this.Posicao=Posicao;
    }
    public int getPosicao()
    {
        return Posicao;
    }
}

